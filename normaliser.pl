#!/usr/bin/perl -w

# Normalise les textes pour le traitement dans la chaîne Presto. Les métadonnées des textes proviennent de 3 sources, par ordre de priorité croissante:
#  1. Les métadonnées présentes dans les textes, sous réserve que le présent script sache les extraire.
#  2. Les métadonnées présentes dans le fichier de métadonnées global passé en paramètre (option -m).
#  3. Les métadonnées forcées dans l'appel au script (option -f).
#
# Paramètres:
#  -d | --debug : mode debug
#  -e <Encoding> | --encoding <Encoding> : codage des fichiers (UTF-8 par défaut)
#  -l <List> | --listMeta <List> : liste des champs de métadonnées qu'on veut garder (séparateur: virgule)
#  -f <JSON> | --forceMeta <JSON> : valeurs de métadonnées à forcer (hashtable au format JSON) en priorité par rapport à toutes les autres sources.
#  -m <Path> | --meta <Path> : chemin vers un fichier de métadonnées. Ces métadonnées sont utilisées en priorité par rapport à celles trouvées dans les textes.
#  -i <ID> | --id <ID> : identifiant du texte dans le fichier de métadonnées
#  -v | --verbose : sortie bavarde
#  --type <Type> : ne traiter qu'un type de fichier
#
# Entrée: le *contenu* d'un texte (par exemple passé par un pipe), généralement accompagné d'un fichier de métadonnées (option -m).
# Sortie: le texte normalisé.

use strict;
use warnings;

use Switch;
use Getopt::Long;
use XML::Entities;
use POSIX;
use Text::CSV_XS qw( csv );
use JSON::XS 'decode_json';
use File::Basename;
use Data::Dumper;  # Utilisé pour le débug

# Tout passer en UTF-8
use v5.14;
use utf8;
binmode STDOUT, ':utf8';
binmode STDIN, ":utf8";  
use open qw(:std :utf8);
use Encode qw(decode_utf8);

# Désactiver les warnings sur les fonctions "expérimentales" (qui marchent très bien).
no if ($] >= 5.018), 'warnings' => 'experimental';


############################
# Traitement des arguments #
############################

# Lecture des arguments
my $config_verbose = 0;
my $config_meta = 0;
my $config_debug = 0;
my $config_fileEncoding = 'utf8';
my $config_forceMeta = '';
my $config_listMeta = '';
my $config_fileName='';
my $config_id='';
my $config_type='';
GetOptions(
  'verbose|v'=>\$config_verbose,
  'meta|m=s'=>\$config_meta,  # Fichier CSV de méta-données
  'listMeta|l=s'=>\$config_listMeta,
  'forcemeta=s'=>\$config_forceMeta,
  'filename=s'=>\$config_fileName,
  'id=s'=>\$config_id,
  'debug|d'=>\$config_debug,  # Mode débug: plante si échec de la normalisation
  'encoding=s'=>\$config_fileEncoding,
  'type=s'=>\$config_type
);

# Construire la liste des métadonnées à conserver
my @listMeta;
if($config_listMeta) {  
  @listMeta = split(/,/, $config_listMeta);
}

# Construire la hashtable des métadonnées à forcer
my %forceMeta;
if($config_forceMeta) {  
  %forceMeta = %{decode_json($config_forceMeta)};
}

# Charger le fichier de métadonnées le cas échéant
my %metadata;  # Hash de hash, utilisable par exemple avec $metadata{'idDuTexte'}{'author'};
if($config_meta) {  
  if(! -e $config_meta) {
    die("Le fichier $config_meta n'existe pas.\n");
  }
  my $tmp = csv(in=>$config_meta, headers=>'auto', sep_char=>"\t") or die Text::CSV_XS->error_diag;
  foreach my $i (keys $tmp) {
    $metadata{$tmp->[$i]{'id'}} = $tmp->[$i];
  }
  
  # Log
  if($config_verbose) {
    print STDERR "[info] Lecture des métadonnées dans le fichier \"$config_meta\".\n";
  }
  if($config_debug) {
    print STDERR Dumper %metadata;
    print STDERR "\n";
  }
}


##############################
# Détection du type de texte #
##############################

my $fileName = basename($config_fileName);  # Nom fichier
my $buffer = '';
my $nbLines = 0;  # Nb de lignes dans le buffer

my $type = 'UNKNOWN';  # Type par défaut

# On essaye de lire les 100 premières lignes
while(($nbLines < 100) && (my $line = <STDIN>)) {
  $buffer .= $line;
  ++$nbLines;
}

# Identification en fonction du nom de fichier et du contenu des 100 premières lignes
# Extension .xml
if($fileName=~m/\.xml$/) {  
  if($buffer=~m/<idno type="FRANTEXT">/i) {  # Frantext
    $type = 'FRANTEXT';
  }
  elsif($buffer=~m/copyright="Bibliothèques Virtuelles Humanistes/i) {  # BVH modifié pour TXM
    $type = 'BVH-mod';
  }
  elsif($buffer=~m;<!--<!DOCTYPE TEI SYSTEM "http://(www.bvh.univ-tours.fr|10.105.50.55):8080/xtf/dtd/renaissance.dtd">-->.+<teiHeader>;is) {  # BVH
    $type = 'BVH';
  }
  elsif($buffer=~m/<distributor>(ACI Astrée|Projet Cyrus)/i) {  # CEPM (l'Astrée et Cyrus)
    $type = 'CEPM';
  }
  elsif($buffer=~m/<p> l'Est Républicain <\/p>/i && $buffer=~m/<name>B. Gaiffe<\/name>/i) {  # Est Républicain
    $type = 'EstRep';
  }
}
# Extension .html
elsif($fileName=~m/\.html$/) {  
  if($fileName!~m/^(\.|~)/ && $buffer=~m;© Université de Poitiers.+Transcription et version html : Pierre Martin;is) {  # BVH téléchargé (HTML)
    $type = 'BVH-HTML';
  }    
}
# Extension -sgml-.txt
elsif($fileName=~m/-sgml\.txt$/) {  
  if($buffer=~m/<text id="/i && $buffer!~m/<issue year="/i) {  # SGML 20ème siècle, sauf presse
    $type = 'SGML20';
  }
  elsif($buffer=~m/<text id="/i && $buffer=~m/<issue year="/i) {  # SGML 20ème siècle, seulement presse
    $type = 'SGML20-press';
  }
  elsif($buffer=~m/<text sig="[^"]+" lemma="/im) {
    $type = 'Encyclo';
  }
}
# Extension .csv
elsif($fileName=~m/\.csv$/) {  
  if($fileName!~m/^(\.|~)/ && $fileName=~m/\.csv$/ && $buffer=~m/Mot n°;Forme rencontrée;Variante de lemme;Lemme Validé;CG Validée/i) {
    $type = 'Analog';                                                    
  }
  elsif($fileName!~m/^(\.|~)/ && $fileName=~m/\.csv$/ && $buffer=~m/^[^\t]+\t[^\t]+\t[^\t]+$/im) {
    $type = 'TTG';
  }
}
# Extension .txt
elsif($fileName=~m/\.txt$/) {
  if($fileName=~m/volume[0-9]+\.txt$/ && $buffer=~m/-+ ARTICLE -+/i) {  # ARTFL (Encyclopédie)
    $type = 'ARTFL';
  }
  elsif($buffer=~m/<text id="[^"]+"[^>]*dateOriginalEdition=/im) {
    $type = 'Presto';
  }
  else {  # Texte brut
    $type = 'Text';
  }
}
# Extension .cha
elsif($fileName=~m/\.cha$/) {
  if($buffer=~m/\@Begin/) {
    $type = 'CHAT';
  }
}

# Ne pas traiter ce type de fichier
if($config_type && $config_type ne $type) {
  print STDERR "[info] Option --type: ne pas traiter les fichiers de type $type.\n";
  exit 1;
}

# Log
if($config_verbose) {
  print STDERR "[info] Texte de type $type\n";
}

# On ne traite pas les fichiers de type inconnu
if($type eq 'UNKNOWN') {
  die("[warning] Impossible d'identifier le fichier $config_fileName; ce fichier ne sera pas traité !\n");
}


##########################
# Normalisation du texte #
##########################

# Charger le reste du fichier. (Attention, on charge tout le fichier ! Travailler sur des fichiers de taille raisonnable.)
my $fileContent = $buffer;  # Texte en cours de normalisation
undef $buffer;  # On n'utilisera plus le buffer; libérer la mémoire.
while(my $line = <STDIN>) {
  $fileContent .= $line;
  ++$nbLines;
}

# Normalisation et extraction des métadonnées du texte en fonction du type de texte
my %intextmetadata = ();  # Métadonnées extraites du texte
switch($type) { 
    
  case 'FRANTEXT' {
    # Extraction des métadonnées
    my $origin = ''; $fileContent=~m!<edition>\s*(.+?)\s*</edition>.+?<publisher>(.+?)</publisher>!s and $origin = "$2 - $1";
    my $licence = ''; $fileContent=~m!<availability.*?>\s*(.+?)\s*</availability>!s and $licence = $1 and $licence=~s/<.*?>//g and $licence=~s/\s+/ /g;
    $intextmetadata{'author'} = $fileContent=~m!<author>\s*(.+?)\s*</author>!s;
    $intextmetadata{'dateOriginalEdition'} = $fileContent=~m!<date>\s*(.+?)\s*</date>!s;
    $intextmetadata{'licence'} = $licence;
    $intextmetadata{'licenceType'} = $licence=~m/<availability status="free">/ ? 'free' : 'nonfree';
    $intextmetadata{'origin'} = $origin;
    $intextmetadata{'source'} = 'Frantext';
    $intextmetadata{'title'} = $fileContent=~m!<title>\s*(.+?)\s*</title>!s;
    $intextmetadata{'type'} = $type;

    # Normalisation
    $fileContent=~s/\0/ /g;  # Remplacer les caractères null par des espaces (normalement il n'y en a pas...)
    $fileContent=~s/\t/ /g;  # Remplacer les tabulations par des espaces (normalement il n'y en a pas...)
    #$fileContent=~s/(<teiHeader>.+?<\/teiHeader>)/substit($1)/ges;  # Échapper la partie "front"        
    #$fileContent=~s/(<(?!\/?(div|p|foreign)[ >])[^>]+?>)//ge;  # Enlever toutes les balises sauf p et div et foreign
    $fileContent=~s/\n+/ /g;  # Enlever les retours chariot

    # Préannotation des Np
    $fileContent=~s!\*([^\s]+)!<presto:pretag pos="Np">$1</presto:pretag>!gs;

    # Préannotation des Xe
    $fileContent=~s!(<foreign(?: [^>]*)?>(.*?)</foreign>)!<presto:pretag pos="Xe">$1</presto:pretag>!gs;

    # Repérage des sections "body", "p", "div", "head" − le document est déjà en TEI, c'est facile − traitement des div0, div1...
    $fileContent=~s!(<(body|p|div|head)[0-9]*(?: |>))!<presto:$2>$1!gm;
    $fileContent=~s!(</(body|p|div|head)[0-9]*>)!$1</presto:$2>!gm;

    # Mise en forme
    $fileContent=~s/(<[^>]+?>)/\n$1\n/g; # Ajouter des retours chariots sur les balises
    $fileContent=~s/\n\s*\n/\n/gs;  # Enlever les lignes vides
    $fileContent=~s/^\s*//gm;  # Enlever les espaces en début de ligne
  }

  case 'BVH-mod' {  # BVH modifié pour TXM
    my $origin = ''; $fileContent=~m!edsci="(.+?)" edcomm="(.+?)"!s and $origin = "$2 - $1"; 
    my $licence = ''; $fileContent=~m!statut="(.+?)"!s and $licence = $1 and $licence=~s/\s+/ /g;
    $intextmetadata{'author'} = $fileContent=~m!auteur="(.+?)"!s;
    $intextmetadata{'licence'} = $licence;
    $intextmetadata{'licenceType'} = $licence=~m/(Creative Commons|CC) BY-NC-SA/ ? 'free' : 'nonfree';
    $intextmetadata{'origin'} = $origin;
    $intextmetadata{'source'} = 'BVH';
    $intextmetadata{'title'} = $fileContent=~m!titre="(.+?)"!s;
    $intextmetadata{'type'} = $type;

    # Normalisation
    $fileContent=~s/&amp;/&/g;
    $fileContent=~s/\0/ /g;  # Remplacer les caractères null par des espaces (normalement il n'y en a pas...)
    $fileContent=~s/\t/ /g;  # Remplacer les tabulations par des espaces (normalement il n'y en a pas...)
    $fileContent=~s!<w [^>]+>(.*?)</w>!$1!gs;  # Enlever les balises <w>
    $fileContent=~s/’/'/gs;  # Normaliser les apostrophes              
    $fileContent=~s/\n+/ /gs;  # Enlever les retours chariot (il y en a partout)

    # Échapper les notes
    $fileContent=~s!(<note.+?</note>)!<presto:out>$1</presto:out>!gs;  

    # Préannotation des Np
    $fileContent=~s!<name(?: [^>]*)>(.+?)</name>!<presto:pretag pos="Np">$1</presto:pretag>!gs;  # NP -> format Frantext

    # Préannotation des Xe
    $fileContent=~s!(<foreign(?: [^>]*)?>(.+?)</foreign>)!<presto:pretag pos="Xe">$1</presto:pretag>!gs;

    # Repérage des sections "body", "p", "div", "head" − le document est déjà en TEI, c'est facile − traitement des div0, div1...
    $fileContent=~s!(<(body|p|div|head)[0-9]*(?: |>))!<presto:$2>$1!gm;
    $fileContent=~s!(</(body|p|div|head)[0-9]*>)!$1</presto:$2>!gm;

    # Mise en forme
    $fileContent=~s/(<[^>]+?>)/\n$1\n/g; # Ajouter des retours chariots sur les balises
    $fileContent=~s/\n\s*\n/\n/gs;  # Enlever les lignes vides
    $fileContent=~s/^\s*//gm;  # Enlever les espaces en début de ligne

    # Cas particulier: les hyphens à l'intérieur d'un mot ne doivent pas être sur une nouvelle ligne
    $fileContent=~s!\n(<lb rend="(?:no)?hyphen[^>]+>)\n(</lb>)\n!$1$2!gis;
    $fileContent=~s!\n(<lb rend="(?:no)?hyphen[^>]+/>)\n!$1!gis;
  }

  case 'BVH' {  
    my $licence = ''; $fileContent=~m!<availability.*?>\s*(.+?)\s*</availability>!s and $licence = $1 and $licence=~s/<.*?>//g and $licence=~s/\s+/ /g;
    $intextmetadata{'author'} = $fileContent=~m!<author>\s*(.+?)\s*</author>!s;
    $intextmetadata{'dateOriginalEdition'} = $fileContent=~m!<sourceDesc>.*?<date>\s*(.+?)\s*</date>!s;
    $intextmetadata{'licence'} = $licence;
    $intextmetadata{'licenceType'} = $licence=~m/CC BY-NC-SA/ ? 'free' : 'nonfree';
    $intextmetadata{'source'} = 'BVH';
    $intextmetadata{'title'} = $fileContent=~m!<title type="titre_court">\s*(.+?)\s*</title>!s;
    $intextmetadata{'type'} = $type;

    # Normalisation
    $fileContent=~s/&amp;/&/g;
    $fileContent=~s/\0/ /g;  # Remplacer les caractères null par des espaces (normalement il n'y en a pas...)
    $fileContent=~s/\t/ /g;  # Remplacer les tabulations par des espaces (normalement il n'y en a pas...)
    $fileContent=~s!<w [^>]+>(.*?)</w>!$1!gs;  # Enlever les balises <w>
    $fileContent=~s/’/'/gs;  # Normaliser les apostrophes              
    $fileContent=~s/\s+/ /gs;  # Enlever les retours chariot et les espaces en trop (il y en a partout)

    # Échapper les notes, les n° de page, les en-têtes de page, corrections
    $fileContent=~s!(<(note|fw|orig|sic)(?: [^>]+)?>.+?</\2>)!<presto:out>$1</presto:out>!gs;  

    # Hyphens
    $fileContent=~s!- <lb rend="((?:no)?hyphen(?:[^>]*?))"/>!<lb rend="$1"/>!gis;

    # Préannotation des Np
    $fileContent=~s!(<(?:sur|fore)?name(?: [^>]*?)?>.+?</(?:sur|fore)?name>)!<presto:pretag pos="Np">$1</presto:pretag>!gs;  # NP -> format Frantext

    # Préannotation des Xe
    $fileContent=~s!(<foreign(?: [^>]*)?>(.+?)</foreign>)!<presto:pretag pos="Xe">$1</presto:pretag>!gs;

    # Repérage des sections "body", "p", "div", "head" − le document est déjà en TEI, c'est facile − traitement des div0, div1...
    $fileContent=~s!(<(body|p|div|head)[0-9]*(?: |>))!<presto:$2>$1!gm;
    $fileContent=~s!(</(body|p|div|head)[0-9]*>)!$1</presto:$2>!gm;

    # Titres
    $fileContent=~s!(<title(?: |>))!<presto:head>$1!gm;
    $fileContent=~s!(</(title>))!$1</presto:head>!gm;

    # Mise en forme
    $fileContent=~s/(<[^>]+?>)/\n$1\n/g; # Ajouter des retours chariots sur les balises
    $fileContent=~s/\n\s*\n/\n/gs;  # Enlever les lignes vides
    $fileContent=~s/^\s*//gm;  # Enlever les espaces en début de ligne

    # Cas particulier: les hyphens à l'intérieur d'un mot ne doivent pas être sur une nouvelle ligne
    $fileContent=~s!\n(<lb rend="hyphen[^>]+>)\n(</lb>)\n!$1$2!gs;
    $fileContent=~s!\n(<lb rend="hyphen[^>]+/>)\n!$1!gs;
  }

  case 'CEPM' {
    my $licence = "Licence Creative Commons : diffusion et reproduction libres avec l'obligation de citer la source et l'interdiction de toute modification et de toute utilisation commerciale";
    $intextmetadata{'author'} = $fileContent=~m!<author>\s*(.+?)\s*</author>!s;
    $intextmetadata{'dateCopyEdition'} = $fileContent=~m!<imprint>.*?<date>\s*([0-9]+)\s*</date>\s*</imprint>!s;
    $intextmetadata{'dateOriginalEdition'} = $fileContent=~m!<edition>\s*<date>\s*([0-9]+)\s*</date>\s*</edition>!s;
    $intextmetadata{'licence'} = $licence;
    $intextmetadata{'licenceType'} = 'free';  # En principe, ils sont d'accord pour qu'on utilise leurs textes, mais nous n'avons jamais réussi à obtenir un accord (ou désaccord) formel.
    $intextmetadata{'source'} = 'CEPM';
    $intextmetadata{'title'} = $fileContent=~m!<title>\s*(.+?)\s*</title>!s;
    $intextmetadata{'type'} = $type;

    # Normalisation
    $fileContent=~s/&amp;/&/g;
    $fileContent=~s/&?nbsp;/ /g;   
    $fileContent=~s/\0/ /g;  # Remplacer les caractères null par des espaces (normalement il n'y en a pas...)
    $fileContent=~s/\t/ /g;  # Remplacer les tabulations par des espaces (normalement il n'y en a pas...)
    $fileContent=~s/’/'/gs;  # Normaliser les apostrophes           

    # Repérage des sections "body", "p", "div", "head" − le document est déjà en TEI, c'est facile − traitement des div0, div1...
    $fileContent=~s!(<(body|p|div|head)[0-9]*(?: |>))!<presto:$2>$1!gm;
    $fileContent=~s!(</(body|p|div|head)[0-9]*>)!$1</presto:$2>!gm;
      
    # Mise en forme
    $fileContent=~s/(<[^>]+?>)/\n$1\n/g; # Ajouter des retours chariots sur les balises
    $fileContent=~s/\n\s*\n/\n/gs;  # Enlever les lignes vides
    $fileContent=~s/^\s*//gm;  # Enlever les espaces en début de ligne

    # Cas particulier: les hyphens à l'intérieur d'un mot ne doivent pas être sur une nouvelle ligne
    $fileContent=~s!\n(<lb rend="hyphen[^>]+>)\n(</lb>)\n!$1$2!gs;
    $fileContent=~s!\n(<lb rend="hyphen[^>]+/>)\n!$1!gs;
  }

  case 'ARTFL' {  
    $intextmetadata{'type'} = $type;
    $intextmetadata{'licenceType'} = $metadata{$config_id}{'Statut_Juridique_Exemplaire'} eq 'libre';  # Un seul volume est libre; vérifier dans le fichier global de métadonnées

    # Normalisation
    $fileContent=~s/\0/ /g;  # Remplacer les caractères null par des espaces (normalement il n'y en a pas...)
    $fileContent=~s/\t/ /g;  # Remplacer les tabulations par des espaces (normalement il n'y en a pas...)
    $fileContent=~s/&(g|l)t;/~$1t;/g;  # Protéger les < >
    $fileContent = XML::Entities::decode('all', $fileContent);
    $fileContent=~s/~(g|l)t;/&$1t;/g;  # Restaurer les < >
    $fileContent=~s/Title Page//;

    $fileContent=~s!-+ ARTICLE -+\s*Lemma=(.*?)\s*Author=(.*?)\s*Normalized Classification=(.*?)\s*Part of Speech=(.*?)\n!</presto:div><presto:div lemma="$1" author="$2" pos="$3">!gs;  # Mettre chaque article dans un div
    $fileContent=~s!^.*?</presto:div>!!s;  # Premier div
    $fileContent.='</presto:div>';  # Dernier div

    # Mise en forme
    $fileContent=~s/\n//gs;  # Enlever les retours à la ligne
    $fileContent=~s/(<[^>]+?>)/\n$1\n/g; # Ajouter des retours chariots sur les balises
    $fileContent=~s/\n\s*\n/\n/gs;  # Enlever les lignes vides
    $fileContent=~s/^\s*//gm;  # Enlever les espaces en début de ligne
  }
  
  case 'Text' {
    $intextmetadata{'licenceType'} = 'unknown';
    $intextmetadata{'source'} = 'unknown';
    $intextmetadata{'title'} = $fileName;
    $intextmetadata{'type'} = $type;

    $fileContent=~s/\0/ /g;  # Remplacer les caractères null par des espaces (normalement il n'y en a pas...)
    $fileContent=~s/\t/ /g;  # Remplacer les tabulations par des espaces (normalement il n'y en a pas...)

    #$fileContent=~s/(\n\n+)/$1<p>/g; # Plusieurs espaces: nouveau paragraphe

    $fileContent=~s/’/'/gs;  # Normaliser les apostrophes
    #$fileContent=~s/E'/É/gs;  # Apostrophes/accents (Apothicaire - bbl_706)
    $fileContent=~s/&amp;/&/gs;  # Entités classiques
      
    #$fileContent=~s/(\n+)/' '.substit($1)/ge; # Échapper les retours chariot (remplacer par un espace)
    $fileContent=~s/(<[^>]+?>)/\n$1\n/g; # Ajouter des retours chariots sur les balises restantes (p et div et foreign)
    
    $fileContent = "<presto:body>\n<presto:div>\n$fileContent\n</presto:div>\n</presto:body>\n";  # Ajout de balises div
  }
  
  else {
    # Type inconnu: ne pas traiter
    print STDERR "[error] Je ne sais pas traiter les fichiers de type '$type' !\n";
    $fileContent = '';  
  }

}


###############
# Métadonnées #
###############

# Calcul des métadonnées finales pour ce texte
my %mymetadata;
if($config_meta) {
  if(! $metadata{$config_id}) {
    print STDERR "[warning] Pas de métadonnées pour $config_id !\n";
  }
  %mymetadata = %{ mergeHashes(\%intextmetadata, \%{$metadata{$config_id}}) };  # Métadonnées du texte utilisées en complément de celles du fichier de métadonnées
}
if($config_forceMeta) {
  %mymetadata = %{ mergeHashes(\%mymetadata, \%forceMeta) };  # Métadonnées forcées dans le script
}


##########
# Sortie #
##########

# Sélectionner les méta-données qu'on veut conserver
my $metastring = '';
foreach my $k (@listMeta) {
  my $optionnal = $k=~m/^\((.*)\)$/;
  if($optionnal) {
    $k = $1;
  }
  if($mymetadata{$k}) {
    $metastring .= " $k=\"".($mymetadata{$k}=~s/"//r)."\"";
  }
  elsif(! $optionnal) {
    print STDERR "[warning] Pas de valeur pour '$k' dans $config_id !\n";
  }
}

# Afficher la sortie
print "<presto:text$metastring>\n";
print $fileContent;
print "</presto:text>\n";


#############
# Fonctions #
#############

# Fusionne deux hashtables. En cas de conflit, le contenu de la seconde écrase celui de la première.
sub mergeHashes {
  my %hash1 = %{$_[0]};
  my %hash2 = %{$_[1]};
  foreach my $k (keys %hash2) {
    if($k && $hash2{$k}) {
      $hash1{$k} = $hash2{$k};
    }
  }
  return \%hash1;
}


