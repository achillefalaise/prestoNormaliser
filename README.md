# Normaliseur

Normalise les textes pour le traitement dans la chaîne Presto. Les métadonnées des textes proviennent de 3 sources, par ordre de priorité croissante:
  1. Les métadonnées présentes dans les textes, sous réserve que le présent script sache les extraire.
  2. Les métadonnées présentes dans le fichier de métadonnées global passé en paramètre (option -m).
  3. Les métadonnées forcées dans l'appel au script (option -f).
  
## Entrée

Ce script traite le *contenu* d'un texte (par exemple, passé par un *pipe*), généralement accompagné d'un fichier de métadonnées (paramètre -m). Ce texte peut être au format texte brut, ou bien dans un des formats pris en charge.
Plusieurs formats basés sur le XML TEI sont pris en charge, mais ce format est relativement vague, et doit donc être précisé en fonction du corpus. C'est pourquoi le XML TEI n'est supporté que pour quelques corpus. L'adaptation à d'autres corpus en XML TEI est assez simple, mais nécessite de modifier le code.

Formats pris en charge:
 * texte brut (pas de méta-données dans le texte)
 * texte brut ARTFL (pour l'Encyclopédie de Diderot & d'Alembert)
 * XML TEI Frantext
 * XML TEI BVH
 * XML TEI CEPM

## Sortie

Une version normalisée du texte, adaptée au traitement à l'aide de *pipes* et de la logithèque GNU.

Le texte normalisé se présente sous forme d'un document XML (si le document d'origine est déjà en XML, ou bien si il est en texte brut), ou pseudo-XML (si le document d'origine n'est pas en XML valide, en HTML non XHTML, etc.; le script ne fait pas de miracles et ne prépare pas le XML; toutefois, la chaîne de traitement Presto se base sur des regex, et non des parseurs DOM ou SAX, et tolère donc assez bien le XML mal formé).

Chaque ligne du texte normalisé contient soit du texte, soit *une* balise. 
Exceptionnellement, une ligne peut contenir du texte *et* des balises, si les balises sont à l'intérieur d'un mot. Quoi qu'il en soit, un mot ne peut *jamais* contenir de retour à la ligne.

Les apostrophes et les espaces sont toutes normalisées (caractères ASCII standards). Les entités XML sont toutes décodées, sauf les caractères `<` et `>`. Les tirets de fin de ligne (*hyphens*) sont supprimés.

Des balises sont ajoutées:
  * `<presto:text>` : balise racine, qui comporte des attributs relatifs aux méta-données, quelles que soient leurs sources (métadonnées présentes dans le texte, dans le fichier global, forcées lors de l'appel au script).
  * `<presto:body>` : corps du texte; le reste est considéré comme du para-texte et ne sera pas inclus dans le corpus.
  * `<presto:div>`, `<presto:p>` : même rôle qu'en HTML ou TEI.
  * `<presto:out>` : des sections à ignorer
  * `<presto:pretag cat="xx">` : des sections à pré-étiqueter (par exemple, certains corpus utilisent des balises ou d'autres systèmes de codage pour repérer les entités nommées ou des mots étrangers; dans ce cas, le script permet de normaliser cette information).

Exemple de sortie:
```xml
<presto:text id="B751131011_Y22789_tei" filePath="Noyau/Periode 2/Le Moyen de parvenir/B751131011_Y22789_tei.xml" niveau="noyau" source="BVH" author="BÉROALDE DE VERVILLE François  " title="Le Moyen de parvenir" dateFirstEdition="1616" genre="genre narratif" dateCopyEdition="1616" licenceType="nonfree" type="BVH" licence="Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 3.0 France (CC BY-NC-SA 3.0 FR) 2014">
<presto:body>
<presto:p>
<p rend="runon">
Je me suis tant 
<lb/>
amusé à vos fadaises de sagesse estant jeu<lb rend="hyphen"/>ne, que j'ay laissé passer les oiseaux. Par 
<lb/>
mon serment, si jamais la paix est faite j'i<lb rend="hyphen"/>ray à la guerre aussi bien que les autres, & 
<lb/>
croyez que si j'eusse sceu ce que je sçay 
<lb/>
maintenant, je fusse dedans; & à ceste heu<lb rend="hyphen"/>re que je sçay le secret, on se défie de moy. 
<lb/>
Que male foire embrenne le nez de ceux 
<lb/>
qui m'ont fait perdre le temps, que cent 
<lb/>
coups de cornes au cul leur dessirent le fon- 
<presto:out>
<fw place="bot-right" type="catch">
dement
</fw>
</presto:out>
<pb n="171" xml:id="B751131011_Y22789_0171"/>
<presto:out>
<fw place="top-center" type="head">
DE PARVENIR
</fw>
</presto:out>
<presto:out>
<fw place="top-right" type="pageNum">
<choice>
<sic>
I79
</sic>
<corr>
I69
</corr>
</choice>
</fw>
</presto:out><lb rend="hyphen"/>dement que puissent - ils devenir coquus 
<lb/>
apres le trespas de leurs femmes de bien. 
<lb/>
Je gage que vous ne sçavez ce que je veux 
<lb/>
dire; ni moy aussi, dit 
<presto:pretag cat="Np">
<name>
Chipon
</name>
</presto:pretag>
</presto:body>
</presto:text>
```

## Algorithme

Le script fonctionne en plusieurs étapes:
  1. Identification du type de texte. Cette identification se fait à l'aide d'une série de *if* qui testent des regex sur le nom du fichier et ses premières lignes. Si toutes les regex associées à un type de texte *matchent*, alors on considère qu'on a identifié le type de texte.
  2. Extraction des méta-données du texte. Il y a une section par type de texte.
  3. Normalisation du texte. Il y a une section par type de texte.

## Paramètres
 * -d | --debug : mode debug
 * -e <Encoding> | --encoding <Encoding> : codage des fichiers (UTF-8 par défaut)
 * -l <List> | --listMeta <List> : liste des champs de métadonnées qu'on veut garder (séparateur: virgule)
 * -f <JSON> | --forceMeta <JSON> : valeurs de métadonnées à forcer (hashtable au format JSON) en priorité par rapport à toutes les autres sources.
 * -m <Path> | --meta <Path> : chemin vers un fichier de métadonnées. Ces métadonnées sont utilisées en priorité par rapport à celles trouvées dans les textes.
 * -i <ID> | --id <ID> : identifiant du texte dans le fichier de métadonnées
 * -v | --verbose : sortie bavarde
 * --type <Type> : ne traiter qu'un type de fichier

Exemple d'utilisation:

`cat montexte.xml | perl normaliser.pl -m "meta.csv" --forcemeta "{\"unchamp\":\"unevaleur\"}" --filename "montexte.xml" --id "unidentifiant" -l "id,filePath"`

## Installation
Ce script en _Perl_ requiert les dépendances suivantes:
 * cpan -i File::Basename
 * cpan -i Getopt::Long
 * cpan -i JSON::XS
 * cpan -i Switch
 * cpan -i Text:CSV_XS
 * cpan -i XML::Entities

## Crédits
Développement: Achille Falaise

Financement:
* projet Presto, ANR, laboratoire ICAR
* labex ASLAN, CNRS, laboratoire ICAR

## Licence
Licence GPL v3
